

require("./bootstrap");
window.Swal = require('sweetalert2');

window.$ = window.jQuery = require("jquery");
window.Vue = require("vue");


Vue.component(
    "cliente-s",
    require("./components/ClienteComponent.vue").default
);

import Vuelidate from "vuelidate"
Vue.use(Vuelidate)
const app = new Vue({
    el: "#app",
    data: {
        menu: 0
    }
});
